import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/navigation/navigation_bloc.dart';
import 'bloc/user_data/user_data_bloc.dart';
import 'presentation/main/main_page.dart';

void main() {
  runApp(GithubApp());
}

class GithubApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<NavigationCubit>(
          create: (BuildContext context) => NavigationCubit(),
        ),
        BlocProvider<UserDataCubit>(
          create: (BuildContext context) => UserDataCubit(),
        ),
      ],
      child: MaterialApp(
        title: 'Github App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MainPage(),
      ),
    );
  }
}
