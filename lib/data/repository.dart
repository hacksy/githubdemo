import 'dart:convert';

import 'package:github_flutter/bloc/user_data/user_data_bloc.dart';
import 'package:http/http.dart' as http;

class Repository {
  static const String BASE_URL = "https://api.github.com/users/";
  Future<User> fetchUser(String user) async {
    final response = await http.get("$BASE_URL$user");
    if (response.statusCode == 200) {
      return User.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('No se pudo cargar el usuario');
    }
  }
}
