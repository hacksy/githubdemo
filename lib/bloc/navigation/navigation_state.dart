part of 'navigation_bloc.dart';

enum NavigationStatus { splash, search, details }

class NavigationState extends Equatable {
  const NavigationState._({
    this.status = NavigationStatus.splash,
    this.data,
  });

  final NavigationStatus status;
  final Map<String, Object> data;

  const NavigationState.splash() : this._(status: NavigationStatus.splash);
  const NavigationState.search() : this._(status: NavigationStatus.search);
  const NavigationState.details(data)
      : this._(status: NavigationStatus.details, data: data);

  @override
  List<Object> get props => [status, data];
}
