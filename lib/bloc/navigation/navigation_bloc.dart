import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'navigation_state.dart';

class NavigationCubit extends Cubit<NavigationState> {
  NavigationCubit() : super(NavigationState.splash());

  void navigateToSearch() async {
    await Future.delayed(Duration(seconds: 4));
    emit(NavigationState.search());
  }

  void navigateToDetails(Map data) async {
    await Future.delayed(Duration(seconds: 4));
    emit(NavigationState.details(data));
  }
}
