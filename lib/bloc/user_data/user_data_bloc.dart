import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_flutter/data/repository.dart';

part 'user_data_state.dart';

class UserDataCubit extends Cubit<UserDataState> {
  UserDataCubit() : super(UserDataState.empty());

  void fetch(String tag) async {
    emit(UserDataState.loading());
    try {
      User user = await Repository().fetchUser(tag);
      emit(UserDataState.loaded(user));
    } catch (e) {
      print(e);
      emit(UserDataState.fail());
    }
  }
}
