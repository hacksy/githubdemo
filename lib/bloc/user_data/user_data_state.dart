part of 'user_data_bloc.dart';

enum UserDataStatus { empty, loading, fail, loaded }

class UserDataState extends Equatable {
  const UserDataState._({
    this.status = UserDataStatus.empty,
    this.user,
  });

  final UserDataStatus status;
  final User user;

  const UserDataState.empty() : this._(status: UserDataStatus.empty);
  const UserDataState.loading() : this._(status: UserDataStatus.loading);
  const UserDataState.fail() : this._(status: UserDataStatus.fail);
  const UserDataState.loaded(user)
      : this._(status: UserDataStatus.loaded, user: user);

  @override
  List<Object> get props => [status, user];
}

class User {
  final String name;
  final String avatarUrl;
  final int repos;
  final int followers;
  User({this.name, this.repos, this.followers, this.avatarUrl});

  factory User.fromJson(Map<String, Object> json) {
    return User(
      name: json["login"] ?? "",
      avatarUrl: json["avatar_url"] ?? "",
      repos: json["public_repos"] ?? 0,
      followers: json["followers"] ?? 0,
    );
  }

  String toString() {
    return "User{name:${name},avatarUrl:${avatarUrl},repos:${repos},followers:${followers}";
  }
}
