import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_flutter/bloc/user_data/user_data_bloc.dart';

class SearchPage extends StatelessWidget {
  String username = "";
  final FocusNode focusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Busqueda de usuario"),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              focusNode: focusNode,
              onChanged: (value) {
                username = value;
              },
            ),
            SizedBox(height: 15),
            RaisedButton(
              onPressed: () {
                focusNode.unfocus();
                BlocProvider.of<UserDataCubit>(context).fetch(username);
              },
              child: Text("Buscar"),
            ),
            SizedBox(height: 15),
            BlocBuilder<UserDataCubit, UserDataState>(
              builder: (context, state) {
                if (state.status == UserDataStatus.loading) {
                  return Center(child: CircularProgressIndicator());
                }
                if (state.status == UserDataStatus.empty) {
                  return SizedBox.shrink();
                }
                if (state.status == UserDataStatus.fail) {
                  return Center(child: Text("No se pudo encontrar el usuario"));
                }
                if (state.status == UserDataStatus.loaded) {
                  return Column(
                    children: [
                      Image.network(state.user.avatarUrl),
                      Text("Nombre de usuario: ${state.user.name}"),
                      Text("Seguidores: ${state.user.followers}"),
                      Text("Numero de repos: ${state.user.repos}"),
                    ],
                  );
                }
                return Container();
              },
            ),
          ],
        ),
      ),
    );
  }
}
