import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_flutter/bloc/navigation/navigation_bloc.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<NavigationCubit>(context).navigateToSearch();
    return Scaffold(
      body: Container(
        color: Colors.grey,
        child: Center(
          child: Image.asset("assets/images/github.png"),
        ),
      ),
    );
  }
}
