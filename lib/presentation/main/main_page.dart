import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_flutter/bloc/navigation/navigation_bloc.dart';
import 'package:github_flutter/bloc/user_data/user_data_bloc.dart';
import 'package:github_flutter/presentation/details/details_page.dart';
import 'package:github_flutter/presentation/search/search_page.dart';
import 'package:github_flutter/presentation/splash/splash_page.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<NavigationCubit, NavigationState>(
      listener: (context, state) {
        if (state.status == NavigationStatus.search) {
          navigateToPage(context, SearchPage());
        }

        if (state.status == NavigationStatus.details) {
          navigateToPage(context, DetailsPage(data: state.data));
        }
      },
      child: SplashPage(),
    );

    //Alternativa
    /*return BlocBuilder<NavigationCubit, NavigationState>(
      cubit: NavigationCubit(),
      builder: (BuildContext context, NavigationState state) {
        switch (state.status) {
          case NavigationStatus.details:
            return DetailsPage();
            break;
          case NavigationStatus.search:
            return SearchPage();
            break;
          case NavigationStatus.splash:
          default:
            return SplashPage();
            break;
        }
      },
    ); */
  }

  void navigateToPage(BuildContext context, Widget widget) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return widget;
    }));
  }
}
